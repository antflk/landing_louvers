window.ll = window.ll || {
};
window.DEBUG_MODE = window.DEBUG_MODE || false;
ll.log = (DEBUG_MODE) ? console.log.bind(console) : function(){};
// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
 
// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
 
// MIT license
 
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
 
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());
ll.comix = function() {
	var $items = $('.comix__view'),
		$list = $('.reviews__list'),
		$line1 = $('.comix__line.first'),
		$line2 = $('.comix__line.second');
	var jumpToComixSlide = function(idx) {
		if (ll.resp.mobileLandscape){
			$items.eq(idx).addClass('active').siblings().removeClass('active');
			$('body').animate({'scrollTop': ll.slides.comix.top});
		} else {
			$items.eq(idx).addClass('active').siblings().removeClass('active');
			$line1.transition({ x: -100*idx + '%'}, 700, 'cubic-bezier(0.305, 0.005, 0.000, 1.000)');
			$line2.transition({ x: -100*idx + '%'}, 760, 'cubic-bezier(0.305, 0.005, 0.000, 1.000)', function(){
				if (idx == 0){
					$line1.removeAttr('style');
					$line2.removeAttr('style');
				}
			});
		}
	};
	$('.comix__link_prev').on('click', function(e) {
		e.preventDefault();
		if (!$(this).hasClass('active')){
			jumpToComixSlide(0);
			$(this).addClass('active');
			$('.comix__link_next').removeClass('active');
		}
	});
	$('.comix__link_next').on('click', function(e) {
		e.preventDefault();
		if (!$(this).hasClass('active')){
			jumpToComixSlide(1);
			$(this).addClass('active');
			$('.comix__link_prev').removeClass('active');
		}
	});
};
ll.contacts = function() {
	var zoomToPath = function(array, map){
		var bounds = new google.maps.LatLngBounds();
		for (var n = 0; n < array.length ; n++){
			bounds.extend(array[n]);
		}
		map.fitBounds(bounds);
		setTimeout(function() {
			google.maps.event.trigger(map,'resize');
		}, 200);
	};
	
	window.initialize_map = function(argument) {
		ll.log('initialize_map')
		var $holder = $('#mapHolder'),
			mapOptions = {
				zoom: 3,
				center: new google.maps.LatLng(0, -180),
				styles: [{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#e0efef"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"hue":"#1900ff"},{"color":"#8EF1DD"}]},{"featureType":"landscape.man_made","elementType":"geometry.fill"},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":100},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","stylers":[{"color":"#73D4C0"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"visibility":"on"},{"lightness":700}]}],
				disableDefaultUI: true,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				zoomControl: true,
				scrollwheel: false
			};
		var map = new google.maps.Map(document.getElementById('mapHolder'), mapOptions);

		var flightPlanCoordinates = [
			new google.maps.LatLng(55.64879046, 37.74419708),
			new google.maps.LatLng(55.64863576, 37.74315639),
			new google.maps.LatLng(55.64842731, 37.74323045),
			new google.maps.LatLng(55.64806632, 37.74087011),
			new google.maps.LatLng(55.64793892, 37.73979722),
			new google.maps.LatLng(55.64753630, 37.73496522),
			new google.maps.LatLng(55.64782752, 37.73491694),
			new google.maps.LatLng(55.64837051, 37.73491157),
			new google.maps.LatLng(55.64867216, 37.73539407)
		];
		var flightPath = new google.maps.Polyline({
			path: flightPlanCoordinates,
			geodesic: true,
			strokeColor: '#00AB8D',
			strokeOpacity: 1.0,
			strokeWeight: 7
		});

		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(55.64871135, 37.73553671),
			map: map,
			icon: '/img/pin.png'
		});

		flightPath.setMap(map);
		flightPlanCoordinates.push(new google.maps.LatLng(55.64902147, 37.73555639));
		// update map sizing and position
		var updateMapSize = function() {
			if (!ll.resp.tablet){
				$holder.height($(window).height() - $('.contacts__footer').height());
			} else if (ll.resp.tablet) {
				$holder.height(0.5 * $(window).width());
			} else {
				$holder.height(1 * $(window).width());
			}
			zoomToPath(flightPlanCoordinates, map);	
		};
		updateMapSize();

		window.ll_map_initiated = true;
		var timeout = 0;
		$(window).on('resize', function() {
			clearTimeout(timeout);
			timeout = setTimeout(updateMapSize, 700);
		});
	}
	var script = document.createElement('script');
	script.type = "text/javascript";
	script.src = "//maps.googleapis.com/maps/api/js?sensor=false&callback=initialize_map";
	document.body.appendChild(script);
};
ll.openPopup = function(type, header, msg) {
	$('.popup__main').children().appendTo('.popup__helpers');
	$('.popup__main').append($('.popup__content.' + type));
	$('.popup').addClass('opened').trigger('opened');
	if (ll.resp.mobileLandscape){
		$('.popup__box').css({top: $(window).height()/2 + $('body').scrollTop() + 'px'});
	};
	if (type === 'feedback'){
		$('.popup__form.feedback').find('input[name=header]').val(header);
	}
	if (type === 'phone'){
		$('.popup__form.phone').find('input[name=header]').val(header);
		$('.popup__form.phone').find('input[name=text]').val(msg);
	}
	if (type === 'approve'){
		$('.popup__title.reponse').html(header);
	}
};
ll.closePopup = function() {
	$('.popup').removeClass('opened').trigger('closed');	
	if (ll.resp.mobileLandscape){
		$('.popup__box').removeAttr('style');
	};
};
ll.popup = function() {
	// Prepare corner svg
	$('.popup__corner').each(function() {
		$(this).data('normal', Snap($(this).children('svg')[0]).select('path').attr('d'));
		$(this).data('active', this.getAttribute('data-altpath'));
		$(this).data('path', Snap($(this).children('svg')[0]).select('path'));
	});
	$('.popup').on('opened', function() {
		$('.popup__corner').data('path')
			.animate({path: $('.popup__corner').data('active')}, 300, mina.easeInElastic);
		$('.popup__close').addClass('front');
	});
	$('.popup').on('closed', function() {
		$('.popup__corner').data('path')
			.animate({path: $('.popup__corner').data('normal')}, 300, mina.easeInElastic);
		$('.popup__close').removeClass('front');
	});
	$('.popup__close').add('.popup__bg').on('click', function() {
		ll.closePopup();
	});

	$('#phone_input2').mask('+7 (999) 999-99-99');

	$('.popupBtn').on('click', function() {
		ll.openPopup(this.getAttribute('data-type'), this.getAttribute('data-header'), this.getAttribute('data-text'));
	});
	$('.popup__form').add('.callme.home__callme').on('submit', function(e) {
		e.preventDefault();
		var $form = $(this),
			$phone = $form.find('input[type=phone]'),
			$email = $form.find('input[type=email]'),
			$text = $form.find('textarea');
		if ($phone.length){
			if($phone.val() === ''){
				$.growl.error({title: '', message: 'Пожалуйста, укажите номер телефона'});
				return false;
			}
		}
		if ($email.length){
			if($email.val() === ''){
				$.growl.error({title: '', message: 'Пожалуйста, укажите адрес электронной почты'});
				return false;
			}
		}
		if ($text.length){
			if($text.val() === ''){
				$.growl.error({title: '', message: 'Пожалуйста, введите текст сообщения'});
				return false;
			}
		}
		$.ajax({
			method: DEBUG_MODE ? 'GET': 'POST',
			url: DEBUG_MODE ? '/response.json' : $form.attr('data-action'),
			data: $form.serialize()
		}).fail(function(xhr, status, error) {
			ll.closePopup();
			$.growl.error({title: status, message: error})
		}).done(function(data) {
			if(data.code == 200){
				ll.openPopup('approve', data.msg);
			} else {
				ll.closePopup();
				$.growl.error({message: data.msg});
			}
		}).always(function(){
			
		});
		return false;
	});
};
ll.resp = {
	tablet: Modernizr.mq('only screen and (max-width:1220px)'),
	mobileLandscape: Modernizr.mq('only screen and (max-width:800px)'),
	mobilePortrait: Modernizr.mq('only screen and (max-width:400px)'),
	touch: Modernizr.touch
};
ll.responsive = function() {
	var timeout = 0;
	$(window).on('resize', function(){
		clearTimeout(timeout);
		timeout = setTimeout(function() {
			ll.resp.tablet = Modernizr.mq('only screen and (max-width:1220px)');
			ll.resp.mobileLandscape = Modernizr.mq('only screen and (max-width:800px)');
			ll.resp.mobilePortrait = Modernizr.mq('only screen and (max-width:400px)');
		}, 100);
	});
};
ll.reviews = function() {
	var $items = $('.reviews__item'),
		$list = $('.reviews__list');
	var jumpToRevSlide = function(idx) {
		$items.filter('.active').removeClass('active');
		$items.eq(idx).addClass('active');
		$list.transition({ x: -2*idx + '%'});
	};
	$('.reviews__prev').on('click', function(e) {
		if ($items.filter('.active').index() === 0) {
			jumpToRevSlide($items.length - 1);
		} else {
			jumpToRevSlide($items.filter('.active').index() - 1);
		}
	});
	$('.reviews__next').on('click', function(e) {
		if ($items.filter('.active').index() === ($items.length - 1)) {
			jumpToRevSlide(0);
		} else {
			jumpToRevSlide($items.filter('.active').index() + 1);
		}
	});
};
ll.slides = {
	home: {
		hash: '#About_us',
		$block: $('.slide.home__slide'),
		top: 0,
		inView: false,
		comingAction: function(){
			// Single action
			if (this.inView) return;
			this.$block.addClass('animate');
			this.$block.find('.showing').addClass('active');
			this.inView = true;
		},
		leavingAction: function() {
		},
		leaveAction: function(){
			this.$block.removeClass('animate');
		}
	},
	comix: {
		hash: '#How_are_we_work',
		$block: $('.comix'),
		top: 0,
		inView: false,
		comingAction: function(){
			// Single
			if (this.inView) return;
			ll.log(this.hash);
			this.inView = true;
		},
		leavingAction: function(){
		},
		leaveAction: function(){
			
		}
	},
	cards: {
		hash: '#Advantages',
		$block: $('.slide.cards__slide'),
		$bg: $('.slide.cards__slide').children('.cards__bg'),
		top: 0,
		inView: false,
		leaving: false,
		comingAction: function(){
			// 
			if (this.inView) return;
			if (ll.scrollTop > (this.top - ll.total_height/2)) {
				this.$bg[0].style.transform = 'translate3d(0,0,0)';
				this.$block.find('.showing').addClass('active');
				this.inView = true;
			}
		},
		leavingAction: function(){
			if (this.leaving) return;
			this.$block.find('.showing').addClass('active');
			this.leaving = true;
		},
		leaveAction: function(){
			ll.log('cards leave')
			this.$bg[0].style.transform = 'translate3d(0,0,0)';
			this.inView = false;
			this.leaving = false;
		}
	},
	reviews: {
		hash: '#Reviews',
		$block: $('.slide.reviews__slide'),
		$bg: $('.slide.reviews__slide').children('.reviews__bg'),
		top: 0,
		inView: false,
		comingAction: function(){
			if (this.inView) return;
			if (ll.scrollTop > (this.top - ll.total_height/2)) {
				ll.log(this.hash);
				this.$block.find('.showing').addClass('active');
				this.inView = true;
			}
		},
		leavingAction: function(){
			if (this.leaving) return;
			this.$bg[0].style.transform = 'translate3d(0,0,0)';
			this.leaving = true;
		},
		leaveAction: function(){
		}
	},
	contacts: {
		hash: '#Contacts',
		$block: $('.slide.contacts__slide'),
		top: 0,
		inView: false,
		comingAction: function(){
			if (this.inView) return;
			this.inView = true;
		},
		leavingAction: function(){
			if (this.leaving) return;
			this.leaving = true;
		},
		leaveAction: function(){
		}
	}
};
ll.slide_names = [
	'home',
	'comix',
	'cards',
	'reviews',
	'contacts'
];
ll.scrollTop = 0;
ll.scrollNew = 0;
ll.total_height = 0;
ll.hash = location.hash;
ll.i = 0;
ll.animate = function(){
	if (ll.scrollNew !== ll.scrollTop){
		for (ll.i = 0; ll.i < 5; ll.i++) {
			// scrollTop + windowHeight >= elTop && scrollTop < elTop
			if (((ll.scrollTop + ll.total_height) >= ll.slides[ll.slide_names[ll.i]].top)
				&&(ll.scrollTop < ll.slides[ll.slide_names[ll.i]].top)) {
				ll.slides[ll.slide_names[ll.i]].comingAction();
			// top >= elTop && top < (elTop + elHeight)
			} else if ((ll.scrollTop >= ll.slides[ll.slide_names[ll.i]].top) 
				&& (ll.scrollTop < (ll.slides[ll.slide_names[ll.i]].top + ll.slides[ll.slide_names[ll.i]].height))) {
				ll.slides[ll.slide_names[ll.i]].leavingAction();
			} else if (ll.slides[ll.slide_names[ll.i]].inView){
				ll.slides[ll.slide_names[ll.i]].leaveAction();
				ll.slides[ll.slide_names[ll.i]].inView = false;
			}
		}
	}
	ll.scrollNew = ll.scrollTop;
	ll.animation = requestAnimationFrame(ll.animate);
};
ll.animation = 0;
ll.slide = function() {
	var body = $('body')[0],
		html = $('html')[0],
		resizeTimeout = 0,
		i = 0,
		// menu
		$menuToggle = $('#menu_toggle'),
		$menuUp = $('.menu__up'),
		$menuDown = $('.menu__down');

	var updateTops = function(){
		if (!ll.resp.tablet){
			$.each(ll.slides, function(){
				this.top = this.$block.position().top | 0;
				this.$block[0].style.height = '';
				this.height = this.$block.height();
				this.$block.height(this.$block.height());
			});
			ll.total_height = $(window).height();
		} else {
			$.each(ll.slides, function(){
				this.top = this.$block.position().top | 0;
				this.$block.removeAttr('style');
			});
			ll.slides.cards.$bg.removeAttr('style');
		}
	};
	$(window).on('resize', function(){
		clearTimeout(resizeTimeout);
		resizeTimeout = setTimeout(updateTops, 500);
	}).trigger('resize');
	$(window).on('load', updateTops);
	var timeout = 0,
		timer = 0,
		nowScrolling = false;
	document.location.hash = ll.slides.home.hash;
	document.location.hash = '';
	$(window).on('scroll', function(e) {
		ll.scrollTop = body.scrollTop || html.scrollTop;
		ll.log(ll.scrollTop);
		if (ll.scrollTop == 0){
			ll.slides.home.comingAction();
		}
		// menu
		if (!ll.mobileLandscape){
			$menuUp[0].style.backgroundPosition = '0 '+ ((ll.scrollTop / 15) | 0) % 20 + 'px';
			$menuDown[0].style.backgroundPosition = '0 '+ (((-ll.scrollTop / 15) | 0) % 20 - 5) + 'px';
		}
		// Check certain slides
		if (!ll.resp.tablet){
			if (ll.scrollNew !== ll.scrollTop){
				for (ll.i = 0; ll.i < 5; ll.i++) {
					// scrollTop + windowHeight >= elTop && scrollTop < elTop
					if (((ll.scrollTop + ll.total_height) >= ll.slides[ll.slide_names[ll.i]].top)
						&&(ll.scrollTop < ll.slides[ll.slide_names[ll.i]].top)) {
						ll.slides[ll.slide_names[ll.i]].comingAction();
					// top >= elTop && top < (elTop + elHeight)
					} else if ((ll.scrollTop >= ll.slides[ll.slide_names[ll.i]].top) 
						&& (ll.scrollTop < (ll.slides[ll.slide_names[ll.i]].top + ll.slides[ll.slide_names[ll.i]].height))) {
						ll.slides[ll.slide_names[ll.i]].leavingAction();
					} else if (ll.slides[ll.slide_names[ll.i]].inView){
						ll.slides[ll.slide_names[ll.i]].leaveAction();
						ll.slides[ll.slide_names[ll.i]].inView = false;
					}
				}
			}
			ll.scrollNew = ll.scrollTop;
		}
		if ($menuToggle[0].className.indexOf('active') !== (-1)){
			$('.menu__icon').trigger('hideMenu');
		}

		clearTimeout(timer);
		if(!body.classList.contains('disable-hover')) {
			body.classList.add('disable-hover')
		}
		timer = setTimeout(function(){
			body.classList.remove('disable-hover')
		},400);
	}).trigger('scroll');

	// Menu animation
	var controlDesktop = function(type, classesMenu, classesItems){
		$menuToggle[type + 'Class'](classesMenu);
		$('.home__phone')[type + 'Class'](classesItems);
		$('.home__content')[type + 'Class'](classesItems);
		$('.comix__view-holder')[type + 'Class'](classesItems);
		$('.comix__nav>.wrp')[type + 'Class'](classesItems);
		$('.cards>.wrp')[type + 'Class'](classesItems);
		$('.reviews>.wrp')[type + 'Class'](classesItems);
	};
	var controlMobile = function(type, classesMenu, classesItems){
		$menuToggle[type + 'Class'](classesMenu);
		$('.slide')[type + 'Class'](classesItems);
		$('.comix')[type + 'Class'](classesItems);
	};
	$('.menu__icon').on('click', function(e) {
		e.preventDefault();
		if (ll.resp.mobileLandscape){
			controlMobile('toggle', 'active', 'moved-to-left');			
		} else {
			controlDesktop('toggle', 'active', 'moved-to-left');
		}
	}).on('hideMenu', function(){
		if (ll.resp.mobileLandscape){
			controlMobile('remove', 'animated active', 'can-move-to-left moved-to-left');
		} else {
			controlDesktop('remove', 'animated active', 'can-move-to-left moved-to-left');
		}
		setTimeout(function(){
			if (ll.resp.mobileLandscape){
				controlMobile('add', 'animated', 'can-move-to-left');
			} else {
				controlDesktop('add', 'animated', 'can-move-to-left');
			}
		}, 100);
	});
	$('.menu__list').on('click', '.menu__link', function(e){
		e.preventDefault();
		$('.menu__icon').trigger('hideMenu');
		for (i = 4; i >= 0; i--) {
			if ((this.getAttribute('href') === '#How_are_we_work') && (!ll.resp.mobileLandscape)){
				$('html,body').animate({scrollTop: ll.slides.comix.$block.height()});
			} else if (this.getAttribute('href') == ll.slides[ll.slide_names[i]].hash){
				$('html,body').animate({scrollTop: ll.slides[ll.slide_names[i]].top});
			}
		};
	});
};
$(function () {
	// Responsive helpers
	ll.responsive();
	
	// Scroll and menu
	ll.slide();
	
	// Home slide input masking
	$('#phone_input').mask('+7 (999) 999-99-99');
	
	// Buttons animations
	$('.featured-input').each(function() {
		$(this).data('normal', Snap($(this).children('svg')[0]).select('path').attr('d'));
		$(this).data('active', this.getAttribute('data-active'));
		$(this).data('path', Snap($(this).children('svg')[0]).select('path'));
	});
	$('.featured-input').on('mousedown', function() {
		$(this).data('path')
			.animate({path: $(this).data('active')}, 1000, mina.elastic);
	}).on('mouseup mouseleave', function() {
		$(this).data('path')
			.animate({path: $(this).data('normal')}, 1000, mina.elastic);
	});

	// Comix
	ll.comix();

	// Reviews slider
	ll.reviews();

	// Popup
	ll.popup();

	// Map
	ll.contacts();
});
//# sourceMappingURL=main.js.map