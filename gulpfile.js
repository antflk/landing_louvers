var gulp = require('gulp'),
    more = {},
    config = require('./gulp/custom.json'),
    connect = require('gulp-connect'),
    notify = require('node-notifier'),
    css = require('./gulp/css'),
    js = require('./gulp/js'),
    svg = require('./gulp/svg'),
    views = require('./gulp/views');

more.config = config;
more.connect = connect;
more.notify = notify;
more.errorHandler = function(error) {
	console.log(error.message || error);
	notify.notify({
		title: 'Error',
		message: error.message || error,
		sound: true,
		icon: __dirname + '/gulp.png'
	})
};

css(gulp, more);   // less,mincss
js(gulp, more);    // js_main,js_plugins,js_build
svg(gulp, more);   // svgstore
views(gulp, more); // jade,connect

var watchActions = function() {
	gulp.watch('./src/less/*', ['less']);
	gulp.watch('./gulp/custom.json', ['js_plugins']);
	gulp.watch('./app/css/main.css', function(){
		gulp.src('./app/css/main.css')
		    .pipe(connect.reload());
	});
	gulp.watch('./src/js/**', ['js_main']);
	gulp.watch('./src/svg/*', ['svgstore']);
	gulp.watch(['./src/views/**', './src/json/**'], ['jade']);
};

gulp.task('default', config.task_list, function(){
	watchActions();
	var spawn = require('child_process').spawn;
	spawn('open', ['http://localhost:3003/']);
});
gulp.task('silent', config.task_list, function(){
	watchActions();
});