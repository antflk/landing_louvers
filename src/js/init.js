$(function () {
	// Responsive helpers
	ll.responsive();
	
	// Scroll and menu
	ll.slide();
	
	// Home slide input masking
	$('#phone_input').mask('+7 (999) 999-99-99');
	
	// Buttons animations
	$('.featured-input').each(function() {
		$(this).data('normal', Snap($(this).children('svg')[0]).select('path').attr('d'));
		$(this).data('active', this.getAttribute('data-active'));
		$(this).data('path', Snap($(this).children('svg')[0]).select('path'));
	});
	$('.featured-input').on('mousedown', function() {
		$(this).data('path')
			.animate({path: $(this).data('active')}, 1000, mina.elastic);
	}).on('mouseup mouseleave', function() {
		$(this).data('path')
			.animate({path: $(this).data('normal')}, 1000, mina.elastic);
	});

	// Comix
	ll.comix();

	// Reviews slider
	ll.reviews();

	// Popup
	ll.popup();

	// Map
	ll.contacts();
});