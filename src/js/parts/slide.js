ll.slides = {
	home: {
		hash: '#About_us',
		$block: $('.slide.home__slide'),
		top: 0,
		inView: false,
		comingAction: function(){
			// Single action
			if (this.inView) return;
			this.$block.addClass('animate');
			this.$block.find('.showing').addClass('active');
			this.inView = true;
		},
		leavingAction: function() {
		},
		leaveAction: function(){
			this.$block.removeClass('animate');
		}
	},
	comix: {
		hash: '#How_are_we_work',
		$block: $('.comix'),
		top: 0,
		inView: false,
		comingAction: function(){
			// Single
			if (this.inView) return;
			ll.log(this.hash);
			this.inView = true;
		},
		leavingAction: function(){
		},
		leaveAction: function(){
			
		}
	},
	cards: {
		hash: '#Advantages',
		$block: $('.slide.cards__slide'),
		$bg: $('.slide.cards__slide').children('.cards__bg'),
		top: 0,
		inView: false,
		leaving: false,
		comingAction: function(){
			// 
			if (this.inView) return;
			if (ll.scrollTop > (this.top - ll.total_height/2)) {
				this.$bg[0].style.transform = 'translate3d(0,0,0)';
				this.$block.find('.showing').addClass('active');
				this.inView = true;
			}
		},
		leavingAction: function(){
			if (this.leaving) return;
			this.$block.find('.showing').addClass('active');
			this.leaving = true;
		},
		leaveAction: function(){
			ll.log('cards leave')
			this.$bg[0].style.transform = 'translate3d(0,0,0)';
			this.inView = false;
			this.leaving = false;
		}
	},
	reviews: {
		hash: '#Reviews',
		$block: $('.slide.reviews__slide'),
		$bg: $('.slide.reviews__slide').children('.reviews__bg'),
		top: 0,
		inView: false,
		comingAction: function(){
			if (this.inView) return;
			if (ll.scrollTop > (this.top - ll.total_height/2)) {
				ll.log(this.hash);
				this.$block.find('.showing').addClass('active');
				this.inView = true;
			}
		},
		leavingAction: function(){
			if (this.leaving) return;
			this.$bg[0].style.transform = 'translate3d(0,0,0)';
			this.leaving = true;
		},
		leaveAction: function(){
		}
	},
	contacts: {
		hash: '#Contacts',
		$block: $('.slide.contacts__slide'),
		top: 0,
		inView: false,
		comingAction: function(){
			if (this.inView) return;
			this.inView = true;
		},
		leavingAction: function(){
			if (this.leaving) return;
			this.leaving = true;
		},
		leaveAction: function(){
		}
	}
};
ll.slide_names = [
	'home',
	'comix',
	'cards',
	'reviews',
	'contacts'
];
ll.scrollTop = 0;
ll.scrollNew = 0;
ll.total_height = 0;
ll.hash = location.hash;
ll.i = 0;
ll.animate = function(){
	if (ll.scrollNew !== ll.scrollTop){
		for (ll.i = 0; ll.i < 5; ll.i++) {
			// scrollTop + windowHeight >= elTop && scrollTop < elTop
			if (((ll.scrollTop + ll.total_height) >= ll.slides[ll.slide_names[ll.i]].top)
				&&(ll.scrollTop < ll.slides[ll.slide_names[ll.i]].top)) {
				ll.slides[ll.slide_names[ll.i]].comingAction();
			// top >= elTop && top < (elTop + elHeight)
			} else if ((ll.scrollTop >= ll.slides[ll.slide_names[ll.i]].top) 
				&& (ll.scrollTop < (ll.slides[ll.slide_names[ll.i]].top + ll.slides[ll.slide_names[ll.i]].height))) {
				ll.slides[ll.slide_names[ll.i]].leavingAction();
			} else if (ll.slides[ll.slide_names[ll.i]].inView){
				ll.slides[ll.slide_names[ll.i]].leaveAction();
				ll.slides[ll.slide_names[ll.i]].inView = false;
			}
		}
	}
	ll.scrollNew = ll.scrollTop;
	ll.animation = requestAnimationFrame(ll.animate);
};
ll.animation = 0;
ll.slide = function() {
	var body = $('body')[0],
		html = $('html')[0],
		resizeTimeout = 0,
		i = 0,
		// menu
		$menuToggle = $('#menu_toggle'),
		$menuUp = $('.menu__up'),
		$menuDown = $('.menu__down');

	var updateTops = function(){
		if (!ll.resp.tablet){
			$.each(ll.slides, function(){
				this.top = this.$block.position().top | 0;
				this.$block[0].style.height = '';
				this.height = this.$block.height();
				this.$block.height(this.$block.height());
			});
			ll.total_height = $(window).height();
		} else {
			$.each(ll.slides, function(){
				this.top = this.$block.position().top | 0;
				this.$block.removeAttr('style');
			});
			ll.slides.cards.$bg.removeAttr('style');
		}
	};
	$(window).on('resize', function(){
		clearTimeout(resizeTimeout);
		resizeTimeout = setTimeout(updateTops, 500);
	}).trigger('resize');
	$(window).on('load', updateTops);
	var timeout = 0,
		timer = 0,
		nowScrolling = false;
	document.location.hash = ll.slides.home.hash;
	document.location.hash = '';
	$(window).on('scroll', function(e) {
		ll.scrollTop = body.scrollTop || html.scrollTop;
		ll.log(ll.scrollTop);
		if (ll.scrollTop == 0){
			ll.slides.home.comingAction();
		}
		// menu
		if (!ll.mobileLandscape){
			$menuUp[0].style.backgroundPosition = '0 '+ ((ll.scrollTop / 15) | 0) % 20 + 'px';
			$menuDown[0].style.backgroundPosition = '0 '+ (((-ll.scrollTop / 15) | 0) % 20 - 5) + 'px';
		}
		// Check certain slides
		if (!ll.resp.tablet){
			if (ll.scrollNew !== ll.scrollTop){
				for (ll.i = 0; ll.i < 5; ll.i++) {
					// scrollTop + windowHeight >= elTop && scrollTop < elTop
					if (((ll.scrollTop + ll.total_height) >= ll.slides[ll.slide_names[ll.i]].top)
						&&(ll.scrollTop < ll.slides[ll.slide_names[ll.i]].top)) {
						ll.slides[ll.slide_names[ll.i]].comingAction();
					// top >= elTop && top < (elTop + elHeight)
					} else if ((ll.scrollTop >= ll.slides[ll.slide_names[ll.i]].top) 
						&& (ll.scrollTop < (ll.slides[ll.slide_names[ll.i]].top + ll.slides[ll.slide_names[ll.i]].height))) {
						ll.slides[ll.slide_names[ll.i]].leavingAction();
					} else if (ll.slides[ll.slide_names[ll.i]].inView){
						ll.slides[ll.slide_names[ll.i]].leaveAction();
						ll.slides[ll.slide_names[ll.i]].inView = false;
					}
				}
			}
			ll.scrollNew = ll.scrollTop;
		}
		if ($menuToggle[0].className.indexOf('active') !== (-1)){
			$('.menu__icon').trigger('hideMenu');
		}

		clearTimeout(timer);
		if(!body.classList.contains('disable-hover')) {
			body.classList.add('disable-hover')
		}
		timer = setTimeout(function(){
			body.classList.remove('disable-hover')
		},400);
	}).trigger('scroll');

	// Menu animation
	var controlDesktop = function(type, classesMenu, classesItems){
		$menuToggle[type + 'Class'](classesMenu);
		$('.home__phone')[type + 'Class'](classesItems);
		$('.home__content')[type + 'Class'](classesItems);
		$('.comix__view-holder')[type + 'Class'](classesItems);
		$('.comix__nav>.wrp')[type + 'Class'](classesItems);
		$('.cards>.wrp')[type + 'Class'](classesItems);
		$('.reviews>.wrp')[type + 'Class'](classesItems);
	};
	var controlMobile = function(type, classesMenu, classesItems){
		$menuToggle[type + 'Class'](classesMenu);
		$('.slide')[type + 'Class'](classesItems);
		$('.comix')[type + 'Class'](classesItems);
	};
	$('.menu__icon').on('click', function(e) {
		e.preventDefault();
		if (ll.resp.mobileLandscape){
			controlMobile('toggle', 'active', 'moved-to-left');			
		} else {
			controlDesktop('toggle', 'active', 'moved-to-left');
		}
	}).on('hideMenu', function(){
		if (ll.resp.mobileLandscape){
			controlMobile('remove', 'animated active', 'can-move-to-left moved-to-left');
		} else {
			controlDesktop('remove', 'animated active', 'can-move-to-left moved-to-left');
		}
		setTimeout(function(){
			if (ll.resp.mobileLandscape){
				controlMobile('add', 'animated', 'can-move-to-left');
			} else {
				controlDesktop('add', 'animated', 'can-move-to-left');
			}
		}, 100);
	});
	$('.menu__list').on('click', '.menu__link', function(e){
		e.preventDefault();
		$('.menu__icon').trigger('hideMenu');
		for (i = 4; i >= 0; i--) {
			if ((this.getAttribute('href') === '#How_are_we_work') && (!ll.resp.mobileLandscape)){
				$('html,body').animate({scrollTop: ll.slides.comix.$block.height()});
			} else if (this.getAttribute('href') == ll.slides[ll.slide_names[i]].hash){
				$('html,body').animate({scrollTop: ll.slides[ll.slide_names[i]].top});
			}
		};
	});
};