ll.comix = function() {
	var $items = $('.comix__view'),
		$list = $('.reviews__list'),
		$line1 = $('.comix__line.first'),
		$line2 = $('.comix__line.second');
	var jumpToComixSlide = function(idx) {
		if (ll.resp.mobileLandscape){
			$items.eq(idx).addClass('active').siblings().removeClass('active');
			$('body').animate({'scrollTop': ll.slides.comix.top});
		} else {
			$items.eq(idx).addClass('active').siblings().removeClass('active');
			$line1.transition({ x: -100*idx + '%'}, 700, 'cubic-bezier(0.305, 0.005, 0.000, 1.000)');
			$line2.transition({ x: -100*idx + '%'}, 760, 'cubic-bezier(0.305, 0.005, 0.000, 1.000)', function(){
				if (idx == 0){
					$line1.removeAttr('style');
					$line2.removeAttr('style');
				}
			});
		}
	};
	$('.comix__link_prev').on('click', function(e) {
		e.preventDefault();
		if (!$(this).hasClass('active')){
			jumpToComixSlide(0);
			$(this).addClass('active');
			$('.comix__link_next').removeClass('active');
		}
	});
	$('.comix__link_next').on('click', function(e) {
		e.preventDefault();
		if (!$(this).hasClass('active')){
			jumpToComixSlide(1);
			$(this).addClass('active');
			$('.comix__link_prev').removeClass('active');
		}
	});
};