ll.resp = {
	tablet: Modernizr.mq('only screen and (max-width:1220px)'),
	mobileLandscape: Modernizr.mq('only screen and (max-width:800px)'),
	mobilePortrait: Modernizr.mq('only screen and (max-width:400px)'),
	touch: Modernizr.touch
};
ll.responsive = function() {
	var timeout = 0;
	$(window).on('resize', function(){
		clearTimeout(timeout);
		timeout = setTimeout(function() {
			ll.resp.tablet = Modernizr.mq('only screen and (max-width:1220px)');
			ll.resp.mobileLandscape = Modernizr.mq('only screen and (max-width:800px)');
			ll.resp.mobilePortrait = Modernizr.mq('only screen and (max-width:400px)');
		}, 100);
	});
};