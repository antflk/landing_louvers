ll.reviews = function() {
	var $items = $('.reviews__item'),
		$list = $('.reviews__list');
	var jumpToRevSlide = function(idx) {
		$items.filter('.active').removeClass('active');
		$items.eq(idx).addClass('active');
		$list.transition({ x: -2*idx + '%'});
	};
	$('.reviews__prev').on('click', function(e) {
		if ($items.filter('.active').index() === 0) {
			jumpToRevSlide($items.length - 1);
		} else {
			jumpToRevSlide($items.filter('.active').index() - 1);
		}
	});
	$('.reviews__next').on('click', function(e) {
		if ($items.filter('.active').index() === ($items.length - 1)) {
			jumpToRevSlide(0);
		} else {
			jumpToRevSlide($items.filter('.active').index() + 1);
		}
	});
};