ll.openPopup = function(type, header, msg) {
	$('.popup__main').children().appendTo('.popup__helpers');
	$('.popup__main').append($('.popup__content.' + type));
	$('.popup').addClass('opened').trigger('opened');
	if (ll.resp.mobileLandscape){
		$('.popup__box').css({top: $(window).height()/2 + $('body').scrollTop() + 'px'});
	};
	if (type === 'feedback'){
		$('.popup__form.feedback').find('input[name=header]').val(header);
	}
	if (type === 'phone'){
		$('.popup__form.phone').find('input[name=header]').val(header);
		$('.popup__form.phone').find('input[name=text]').val(msg);
	}
	if (type === 'approve'){
		$('.popup__title.reponse').html(header);
	}
};
ll.closePopup = function() {
	$('.popup').removeClass('opened').trigger('closed');	
	if (ll.resp.mobileLandscape){
		$('.popup__box').removeAttr('style');
	};
};
ll.popup = function() {
	// Prepare corner svg
	$('.popup__corner').each(function() {
		$(this).data('normal', Snap($(this).children('svg')[0]).select('path').attr('d'));
		$(this).data('active', this.getAttribute('data-altpath'));
		$(this).data('path', Snap($(this).children('svg')[0]).select('path'));
	});
	$('.popup').on('opened', function() {
		$('.popup__corner').data('path')
			.animate({path: $('.popup__corner').data('active')}, 300, mina.easeInElastic);
		$('.popup__close').addClass('front');
	});
	$('.popup').on('closed', function() {
		$('.popup__corner').data('path')
			.animate({path: $('.popup__corner').data('normal')}, 300, mina.easeInElastic);
		$('.popup__close').removeClass('front');
	});
	$('.popup__close').add('.popup__bg').on('click', function() {
		ll.closePopup();
	});

	$('#phone_input2').mask('+7 (999) 999-99-99');

	$('.popupBtn').on('click', function() {
		ll.openPopup(this.getAttribute('data-type'), this.getAttribute('data-header'), this.getAttribute('data-text'));
	});
	$('.popup__form').add('.callme.home__callme').on('submit', function(e) {
		e.preventDefault();
		var $form = $(this),
			$phone = $form.find('input[type=phone]'),
			$email = $form.find('input[type=email]'),
			$text = $form.find('textarea');
		if ($phone.length){
			if($phone.val() === ''){
				$.growl.error({title: '', message: 'Пожалуйста, укажите номер телефона'});
				return false;
			}
		}
		if ($email.length){
			if($email.val() === ''){
				$.growl.error({title: '', message: 'Пожалуйста, укажите адрес электронной почты'});
				return false;
			}
		}
		if ($text.length){
			if($text.val() === ''){
				$.growl.error({title: '', message: 'Пожалуйста, введите текст сообщения'});
				return false;
			}
		}
		$.ajax({
			method: DEBUG_MODE ? 'GET': 'POST',
			url: DEBUG_MODE ? '/response.json' : $form.attr('data-action'),
			data: $form.serialize()
		}).fail(function(xhr, status, error) {
			ll.closePopup();
			$.growl.error({title: status, message: error})
		}).done(function(data) {
			if(data.code == 200){
				ll.openPopup('approve', data.msg);
			} else {
				ll.closePopup();
				$.growl.error({message: data.msg});
			}
		}).always(function(){
			
		});
		return false;
	});
};