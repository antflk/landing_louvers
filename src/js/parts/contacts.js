ll.contacts = function() {
	var zoomToPath = function(array, map){
		var bounds = new google.maps.LatLngBounds();
		for (var n = 0; n < array.length ; n++){
			bounds.extend(array[n]);
		}
		map.fitBounds(bounds);
		setTimeout(function() {
			google.maps.event.trigger(map,'resize');
		}, 200);
	};
	
	window.initialize_map = function(argument) {
		ll.log('initialize_map')
		var $holder = $('#mapHolder'),
			mapOptions = {
				zoom: 3,
				center: new google.maps.LatLng(0, -180),
				styles: [{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#e0efef"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"hue":"#1900ff"},{"color":"#8EF1DD"}]},{"featureType":"landscape.man_made","elementType":"geometry.fill"},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":100},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","stylers":[{"color":"#73D4C0"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"visibility":"on"},{"lightness":700}]}],
				disableDefaultUI: true,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				zoomControl: true,
				scrollwheel: false
			};
		var map = new google.maps.Map(document.getElementById('mapHolder'), mapOptions);

		var flightPlanCoordinates = [
			new google.maps.LatLng(55.64879046, 37.74419708),
			new google.maps.LatLng(55.64863576, 37.74315639),
			new google.maps.LatLng(55.64842731, 37.74323045),
			new google.maps.LatLng(55.64806632, 37.74087011),
			new google.maps.LatLng(55.64793892, 37.73979722),
			new google.maps.LatLng(55.64753630, 37.73496522),
			new google.maps.LatLng(55.64782752, 37.73491694),
			new google.maps.LatLng(55.64837051, 37.73491157),
			new google.maps.LatLng(55.64867216, 37.73539407)
		];
		var flightPath = new google.maps.Polyline({
			path: flightPlanCoordinates,
			geodesic: true,
			strokeColor: '#00AB8D',
			strokeOpacity: 1.0,
			strokeWeight: 7
		});

		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(55.64871135, 37.73553671),
			map: map,
			icon: '/img/pin.png'
		});

		flightPath.setMap(map);
		flightPlanCoordinates.push(new google.maps.LatLng(55.64902147, 37.73555639));
		// update map sizing and position
		var updateMapSize = function() {
			if (!ll.resp.tablet){
				$holder.height($(window).height() - $('.contacts__footer').height());
			} else if (ll.resp.tablet) {
				$holder.height(0.5 * $(window).width());
			} else {
				$holder.height(1 * $(window).width());
			}
			zoomToPath(flightPlanCoordinates, map);	
		};
		updateMapSize();

		window.ll_map_initiated = true;
		var timeout = 0;
		$(window).on('resize', function() {
			clearTimeout(timeout);
			timeout = setTimeout(updateMapSize, 700);
		});
	}
	var script = document.createElement('script');
	script.type = "text/javascript";
	script.src = "//maps.googleapis.com/maps/api/js?sensor=false&callback=initialize_map";
	document.body.appendChild(script);
};