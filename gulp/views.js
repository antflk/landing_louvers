module.exports = function(gulp, m) {
	var jade = require('gulp-jade'),
	    config = require('./custom.json'),
	    obj = require('../src/json/data.json');

	gulp.task('jade', function() {
		delete require.cache[__dirname + '/src/json/data.json'];
		obj = require('../src/json/data.json');
		gulp.src('./src/views/index.jade')
		    .pipe(jade({
		    	pretty: true,
		    	locals: obj
		    }).on('error', m.errorHandler))
		    .pipe(gulp.dest('./app'))
		    .pipe(m.connect.reload());
	});

	gulp.task('connect', function () {
		m.connect.server({
			root: ['app'],
			port: 3003,
			livereload: true
		});
	});
};