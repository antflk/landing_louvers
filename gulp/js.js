module.exports = function(gulp, m) {
	var uglify = require('gulp-uglify'),
	    sourcemaps = require('gulp-sourcemaps'),
	    rename = require('gulp-rename'),
	    concat = require('gulp-concat'),
	    config = require('./custom.json');
	
	gulp.task('js_main', function(){
		gulp.src(['./src/js/_define.js','./src/js/*/*.js','./src/js/init.js'])
		    .pipe(sourcemaps.init())
		    .pipe(concat('main.js'))
		    .pipe(sourcemaps.write('.'))
		    .pipe(gulp.dest('./app/js'))
		    .pipe(m.connect.reload())
		    .on('error', function(error) {
		    	m.notify.notify({
		    		title: "Error",
		    		message: error.message
		    	});
		    });
	});
	gulp.task('js_plugins', function() {
		delete require.cache[__dirname + '/custom.json'];
		config = require('./custom.json');
		gulp.src(config.js_plugins)
			.pipe(sourcemaps.init())
			.pipe(concat('plugins.js'))
		    .pipe(sourcemaps.write('.'))
		    .pipe(gulp.dest('./app/js'));
	});
	gulp.task('js_build', function() {
		gulp.src('./app/js/main.js')
		    .pipe(uglify())
		    .pipe(rename('main.min.js'))
		    .pipe(gulp.dest('./app/js'));
		gulp.src('./app/js/plugins.js')
		    .pipe(uglify())
		    .pipe(rename('plugins.min.js'))
		    .pipe(gulp.dest('./app/js'));
	});
};