module.exports = function(gulp, m) {
	var svgstore = require('gulp-svgstore'),
	    svgmin = require('gulp-svgmin'),
	    raster = require('gulp-raster'),
	    rename = require('gulp-rename');
	
	gulp.task('svgstore', function() {
		gulp.src('./src/svg/*.svg')
		    .pipe(svgmin())
		    .pipe(svgstore({
		    	fileName: 'icons.svg',
		    	transformSvg: function ($svg, done) {
					$svg.find('[fill]').removeAttr('fill')
					done(null, $svg)
				}
		    }))
		    .pipe(gulp.dest('./app/img'))
		    .pipe(m.connect.reload());
	});

	gulp.task('svg-fallback', function(){
		return gulp
		        .src('src/svg/*.svg', {base: 'src/svg'})
		        .pipe(raster())
		        .pipe(rename(function (path) {
		        	var name = path.dirname.split(path.sep);
		        	name.push(path.basename);
		        	path.basename = 'icons.svg.' + name[1];
		        	path.extname = '.png';
		        }))
		        .pipe(gulp.dest('app/img'));
	});
};