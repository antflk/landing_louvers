module.exports = function(gulp, m) {
	var less = require('gulp-less'),
	    concat = require('gulp-concat'),
	    sourcemaps = require('gulp-sourcemaps'),
	    autoprefixer = require('gulp-autoprefixer'),
	    cleancss = require('gulp-clean-css'),
	    rename = require('gulp-rename');

	gulp.task('less', function() {
		gulp.src(m.config.css_list)
		    .pipe(sourcemaps.init())
		    .pipe(less())
		    .on('error', m.errorHandler)
		    .pipe(concat('main.css'))
		    .on('error', m.errorHandler)
		    .pipe(autoprefixer())
		    .on('error', m.errorHandler)
		    .pipe(sourcemaps.write('.'))
		    .on('error', m.errorHandler)
		    .pipe(gulp.dest('./app/css'))
		    .on('error', m.errorHandler);
	});
	gulp.task('mincss', function() {
		gulp.src('./app/css/main.css')
		    .pipe(cleancss())
		    .pipe(rename('main.min.css'))
		    .pipe(gulp.dest('./app/css'));
	});
};